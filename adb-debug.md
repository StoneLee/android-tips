本文简要介绍了如何使用adb进行debug。

## 1. 连接手机与PC

在开始工作之前，你需要将手机正确连接到PC。有两种方式：

### 1.1. 使用USB

将手机通过USB线连接到PC即可。此种方式需要安装ADB驱动。一般手机厂商提供的随机软件应该会自动安装该驱动。

### 1.2. Wifi（TCP/IP）

先将手机与PC均连到同一个无线网络下，然后在手机的WLAN界面下找到该手机的IP。在命令行执行`
adb connect IP`

可以使用以下命令查看手机是否已与PC正确建立连接：
`adb wait-for-device`。
如果该命令行顺利返回，则表示连接正常，adb可正常使用。

## 2. 获取ANR或程序崩溃时的系统日志

测试开始之前，在控制台里执行：
```
adb root
adb shell 'rm -rf data/anr data/tombstones data/bugreports/×'
adb logcat -c
```

测试时遇到app意外退出或无响应，在控制台里执行
```
adb root
adb logcat -d > log.txt
adb pull data/anr
adb pull data/tombstones
adb pull data/bugreports
```
